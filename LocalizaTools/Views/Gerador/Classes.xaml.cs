﻿using EnvDTE;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LocalizaTools.Views.Gerador
{
    /// <summary>
    /// Interaction logic for Classes.xaml
    /// </summary>
    public partial class Classes : System.Windows.Window
    {
        private readonly DTE _dte;
        public Classes(DTE dte)
        {
            _dte = dte;
            InitializeComponent();

        }

        public void GerarClasses(string nomePasta, string nomeClasse, string tipoClasse)
        {
            var solution = _dte.Solution;

            //Percorre os projetos da solução aberta.
            foreach (Project item in solution.Projects)
            {
                var caminhoTempArquivoCriado = "";
                var nomePastaNoProjeto = $"Administrar{nomePasta}";
                var nomeNamespace = $"{item.Name}.{nomePasta}";

                if (item.Name.Contains("Fronteiras") && (tipoClasse == "Executor" || tipoClasse == "IRepositorio"))
                {
                    if (tipoClasse == "Executor")
                    {
                        CriarPastaNoProjeto(item, "Executores", $"{nomePastaNoProjeto}");
                        caminhoTempArquivoCriado = GerarRequisicao($"{item.Name}.Executores.{nomePastaNoProjeto}", nomeClasse);
                        item.ProjectItems.Item("Executores").ProjectItems.Item(nomePastaNoProjeto).ProjectItems.AddFromFile(caminhoTempArquivoCriado);
                    }
                }
                else if(item.Name.Contains("Executores") && tipoClasse == "Executor")
                {
                    CriarPastaNoProjeto(item, nomePastaNoProjeto);
                    caminhoTempArquivoCriado = GerarExecutor(nomeNamespace, nomeClasse);
                    item.ProjectItems.Item(nomePastaNoProjeto).ProjectItems.AddFromFile(caminhoTempArquivoCriado);
                }

                item.Save();
            }

            var resultado = MessageBox.Show("Arquivo criado com sucesso. Deseja criar mais arquivos?", "Sucesso", MessageBoxButton.YesNo);

            if(resultado == MessageBoxResult.No)
            {
                this.Close();
            }
        }

        public void CriarPastaNoProjeto(Project projeto, string nomePasta, string subPasta = "")
        {
            var caminhoCompleto = System.IO.Path.GetDirectoryName(projeto.FullName);
            caminhoCompleto = System.IO.Path.Combine(caminhoCompleto, nomePasta);

            if(!System.IO.Directory.Exists(caminhoCompleto))
            {
                projeto.ProjectItems.AddFolder(nomePasta);
                projeto.Save();
            }

            if (!string.IsNullOrEmpty(subPasta))
            {
                var caminhoSubPasta = System.IO.Path.Combine(caminhoCompleto, subPasta);
                if (!System.IO.Directory.Exists(caminhoSubPasta))
                {
                    projeto.ProjectItems.Item(nomePasta).ProjectItems.AddFolder(subPasta);
                    projeto.Save();
                }
            }
        }

        private void btnGerarClasse_Click(object sender, RoutedEventArgs e)
        {
            GerarClasses(txtNomePasta.Text, txtNomeClasse.Text, cboTipoClasse.Text);
        }

        private string GerarExecutor(string namespaceClasse, string nomeClasse)
        {
            var nomeClasseRequisicao = nomeClasse;
            var nomeClasseResultado = $"{nomeClasse}";

            //Executor
            var classe = Templates.Executores.Executor.GerarExecutor(namespaceClasse, nomeClasse, nomeClasseRequisicao, nomeClasseResultado);
            
            var tempPath = System.IO.Path.GetTempPath();
            var caminhoArquivoClasse = System.IO.Path.Combine(tempPath, $"{nomeClasse}Executor.cs");
            System.IO.File.WriteAllText(caminhoArquivoClasse, classe);

            //TODO: verificar uma forma de adicionar ao projeto sem criar um arquivo temporário

            return caminhoArquivoClasse;
        }

        private string GerarRequisicao(string namespaceClasse, string nomeClasse)
        {
            var nomeClasseRequisicao = nomeClasse;

            //Executor
            var classe = Templates.Fronteiras.Requisicao.Gerar(namespaceClasse, nomeClasseRequisicao);

            var tempPath = System.IO.Path.GetTempPath();
            var caminhoArquivoClasse = System.IO.Path.Combine(tempPath, $"{nomeClasseRequisicao}Requisicao.cs");
            System.IO.File.WriteAllText(caminhoArquivoClasse, classe);

            //TODO: verificar uma forma de adicionar ao projeto sem criar um arquivo temporário

            return caminhoArquivoClasse;
        }

        private string GerarResultado(string namespaceClasse, string nomeClasse)
        {
            var nomeClasseResultado = nomeClasse;

            //Executor
            var classe = Templates.Fronteiras.Requisicao.Gerar(namespaceClasse, nomeClasse);

            var tempPath = System.IO.Path.GetTempPath();
            var caminhoArquivoClasse = System.IO.Path.Combine(tempPath, $"{nomeClasseResultado}Resultado.cs");
            System.IO.File.WriteAllText(caminhoArquivoClasse, classe);

            //TODO: verificar uma forma de adicionar ao projeto sem criar um arquivo temporário

            return caminhoArquivoClasse;
        }
    }
}

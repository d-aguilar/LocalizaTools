﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizaTools.Templates.Fronteiras
{
    public partial class Requisicao
    {
        private readonly string _namespace;
        private readonly string _nomeClasse;
        public Requisicao(string namespaceClasse, string nomeClasse)
        {
            _namespace = namespaceClasse;
            _nomeClasse = nomeClasse;
        }

        public static string Gerar(string namespaceClasse, string nomeClasse)
        {
            var classe = new Requisicao(namespaceClasse, nomeClasse).TransformText();
            return classe;
        }
    }
}

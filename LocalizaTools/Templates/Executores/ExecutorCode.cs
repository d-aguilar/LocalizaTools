﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizaTools.Templates.Executores
{
    partial class Executor
    {
        private string _namespace;
        private string _nomeClasse;
        private string _requisicao;
        private string _resultado;

        public Executor(string namespaceClasse, string nomeClasse, string requisicao, string resultado)
        {
            _namespace = namespaceClasse;
            _nomeClasse = nomeClasse;
            _requisicao = requisicao;
            _resultado = resultado;
        }

        public static string GerarExecutor(string namespaceClasse, string nomeClasse, string requisicao, string resultado)
        {
            var classe = new Executor(namespaceClasse, nomeClasse, requisicao, resultado).TransformText();
            return classe;
        }

    }
}
